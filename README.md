# Gladys Deezer

Gladys Deezer allow Gladys to connect to your Premium account to play music !

## Getting Started

### Prequisites

- First, you have to create an app in [Deezer for Developers](https://developers.deezer.com/)
- Memorize your IDs !
- Create parameters :
    - DEEZER_CLIENT_ID : "The ID of your Deezer App"
    - DEEZER_CLIENT_SECRET : "The secret key of your Deezer App"
    - DEEZER_REDIRECT_URI : "http://[the DNS of your RPi]:[the port of Gladys]"

### Installation

- Install this module in Gladys
- Reboot Gladys
- Check Gladys' logs
- You should see an URL like "https://connect.deezer.com/oauth/[...]"
- Copy and paste it on your browser and follow the instructions
- You will be redirected on the redirect URI, with a "code" parameter.
- Create a parameter in Gladys :
    - DEEZER_CODE : "The code"
- On the "Module" view, click on the Setting button of this module
- Check Gladys' logs
- You should see an URL like "https://connect.deezer.com/oauth/[...]"
- Copy and paste it on your browser and follow the instructions
- You will now get the access token !
- Create a new parameter :
    - DEEZER_TOKEN : "Your token"


## Credits

Thanks to [AdrienDesola](https://developer.gladysproject.com/fr/modules/spotify-connect) for the Spotify version. This module is directly inspired on him :)