const schedule = require('node-schedule');
const util = require('util');
const shared = require("./shared");
var Promise = require('bluebird');

module.exports = function init() {
    return gladys
        .param
        .getValues(['DEEZER_CLIENT_ID', 'DEEZER_REDIRECT_URI'])
        .then(values => {
            const clientId = values[0],
                redirectUri = values[1];

            // Generate to login URL with the informations of your app
            const url = getLoginUrl(clientId, redirectUri, shared.perms);

            console.info(`**** \n Allow your Deezer Account by click on this url : \n\n ${url} \n\n ****`);

            return shared.player;
        });
}


// Function to generate a login URL

const getLoginUrl = function (clientId, redirectUri, perms) {
    let URL = "https://connect.deezer.com/oauth/auth.php?app_id=" + clientId + "&redirect_uri=" + redirectUri + "&perms=";
    for (let i = 0; i < perms.length; i++) {
        URL += perms[i];
        if (i < perms.length - 1) {
            URL += ",";
        }
    }
    return URL;
}