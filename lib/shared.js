module.exports = {
    player: null,
    perms: [
        'basic_access',
        'email',
        'offline_access',
        'manage_library',
        'manage_community',
        'delete_library',
        'listening_history'
    ]
}