const init = require('./init');
const shared = require('./shared');

module.exports = function setup() {
    return gladys
        .param
        .getValues(['DEEZER_CLIENT_ID', 'DEEZER_CLIENT_SECRET', 'DEEZER_CODE'])
        .then(values => {
            const clientId = values[0],
                clientSecret = values[1],
                code = values[2];

            // Generate to login URL with the informations of your app
            const url = getTokenURL(clientId, clientSecret, code);

            console.info(`**** \n Get your Deezer Access Token by click on this url : \n\n ${url} \n\n ****`);

            return shared.player;
        });
}

// Function to generate a login URL

const getTokenURL = function (clientId, clientSecret, code) {
    return "https://connect.deezer.com/oauth/access_token.php?app_id=" + clientId + "&secret=" + clientSecret + "&code=" + code;
}